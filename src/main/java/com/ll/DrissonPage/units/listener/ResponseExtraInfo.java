package com.ll.drissonPage.units.listener;

import java.util.List;
import java.util.Map;

/**
 * @author 陆
 * @address <a href="https://t.me/blanksig"/>click
 */
public class ResponseExtraInfo  extends ExtraInfo{
    public String requestId;
    public List<Map<String, Object>> blockedCookies;
    public String resourceIPAddressSpace;
    public int statusCode;
    public String headersText;
    public String cookiePartitionKey;
    public boolean cookiePartitionKeyOpaque;
    public ResponseExtraInfo(Map<String, Object> extraInfo) {
        super(extraInfo);
    }
}
