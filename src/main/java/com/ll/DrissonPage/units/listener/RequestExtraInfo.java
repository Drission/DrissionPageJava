package com.ll.drissonPage.units.listener;

import java.util.List;
import java.util.Map;

/**
 * @author 陆
 * @address <a href="https://t.me/blanksig"/>click
 */
public class RequestExtraInfo extends ExtraInfo {
    public String requestId;
    public List<Map<String, Object>> associatedCookies;
    public Map<String, Object> headers;
    public Map<String, Object> connectTiming;
    public Map<String, Object> clientSecurityState;
    public boolean siteHasCookieInOtherPartition;

    public RequestExtraInfo(Map<String, Object> extraInfo) {
        super(extraInfo);
    }
}
